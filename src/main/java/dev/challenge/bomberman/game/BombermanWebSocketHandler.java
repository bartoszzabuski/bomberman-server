/*
 * Copyright 2012-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */

package dev.challenge.bomberman.game;

import static java.lang.String.format;
import static java.util.Collections.synchronizedList;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

public class BombermanWebSocketHandler extends TextWebSocketHandler {

    private final List<Player> players = synchronizedList(new ArrayList());

    public BombermanWebSocketHandler() {
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws IOException {
        System.out.println("connection established: " + session);
        Player player = new Player(session);
        players.add(player);
        sendShipIdToClient(player);
    }

    private void sendShipIdToClient(final Player player) throws IOException {
        player.getSession().sendMessage(new TextMessage("h1-" + player.getId()));
    }

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message)
            throws Exception {
        String payload = message.getPayload();
        System.out.println("received message: " + payload.toString());
        if (payload.contains("clientReady")) {
            for (Player player : players) {
                for (Player player1 : players) {
                    player.getSession().sendMessage(new TextMessage(format("h2-%s", player1.getId())));
                }
            }
        } else {
            System.err.println("ship's state message");
            for (Player player : players) {
                System.out.println("echoing inputs to: " + player.getSession());
                player.getSession().sendMessage(new TextMessage(payload));
            }
        }
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception)
            throws Exception {
        System.out.println("error " + exception.getMessage());
        session.close(CloseStatus.SERVER_ERROR);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        System.out.println("connection closed: " + status);
        removePlayerWithSession(session);
    }

    private void removePlayerWithSession(final WebSocketSession session) throws IOException {
        Iterator<Player> iterator = players.iterator();
        while (iterator.hasNext()) {
            Player player = iterator.next();
            if (player.getSession().equals(session)) {
                System.err.println("removing player with id: " + player.getId());
                iterator.remove();
                break;
            }
        }
        for (Player player : players) {
            player.getSession().sendMessage(new TextMessage(format("removeShip-%s", player.getId())));
        }
    }

}
