package dev.challenge.bomberman.game;

import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.web.socket.WebSocketSession;

public class Player {

    private static final AtomicInteger idGenerator = new AtomicInteger(0);

    private final Integer id;
    private final WebSocketSession session;

    public Player(final WebSocketSession session) {
        this.id = idGenerator.incrementAndGet();
        this.session = session;
    }

    public Integer getId() {
        return id;
    }

    public WebSocketSession getSession() {
        return session;
    }
}
